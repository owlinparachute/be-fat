from flask import Flask

from befat_admin.admin import setup_admin
from befat_admin.views import admin_handlers
from befat.api import api_bp
from extensions import db
from extensions import login_manager
from extensions import api

app = Flask(__name__)
#app.config["MONGODB_SETTINGS"] = get_config()['MONGODB_SETTINGS']
app.config["MONGODB_SETTINGS"] = dict(
        db = 'befat',
        username = '',
        password = '',
        host = 'db',
        port = 27017)
app.config['SECRET_KEY'] = 'judybangbang'
app.config['BUNDLE_ERRRORS'] = True

setup_admin(app)
db.init_app(app)
login_manager.init_app(app)
api.init_app(api_bp)

app.register_blueprint(admin_handlers)
app.register_blueprint(api_bp)


if __name__ == "__main__":
    #print app.url_map
    app.run(host='0.0.0.0', debug=True)
