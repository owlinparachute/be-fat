#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

__version__ = '0.1.0'
readme = open('README.rst').read()

setup(
    name='be-fat',
    version=__version__,
    description='',
    long_description=readme,
    author='Judy Y.',
    author_email='owlinparachute@gmail.com',
    packages=find_packages(),
    package_dir={'be-fat': 'be-fat'},
)
