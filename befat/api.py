# -*- coding: utf-8 -*-

from flask import Flask
from flask import request
from flask import jsonify
from flask import Blueprint
from flask import abort, redirect, url_for
from flask_restful import Resource, reqparse



from extensions import api
from extensions import db
from models import User
from models import UserInfo
from models import Measurement

api_bp = Blueprint('api', __name__, url_prefix='/api')

#@app.errorhandler(404)
#def not_found(error):
    #return make_response(jsonify({'error': 'Not found'}), 404)


class UserResource(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('username', type=str, required=True)
        self.parser.add_argument('password', type=str, required=True)
        super(UserResource, self).__init__()

    def get(self, user_id):
        user = User.objects.get(user_id)
        if not user:
            abort(400)
        return jsonify({'username': user.username})

    def post(self):
        args = self.parser.parse_args()
        username = args['username']
        password = args['password']
        if username is None or password is None:
            abort(400) # missing arguments
        if User.objects(username = username).first() is not None:
            abort(400) # missing arguments
        user = User(username = username)
        user.set_password(password)
        user.save()
        response = jsonify({ 'username': user.username, 'user_id': str(user.id) })
        response.status_code = 201
        return response

    def put(self):
        # edit user info.


class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}


api.add_resource(HelloWorld, '/')
api.add_resource(UserResource, '/user')
