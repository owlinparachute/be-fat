# -*- coding: utf-8 -*-

from flask import Flask
from flask.ext.mongoengine import MongoEngine
from flask.ext.login import LoginManager
from flask.ext.admin import Admin
from flask_restful import Api

admin = Admin(name=u'Be Fat Admin')
api = Api()
db = MongoEngine()
login_manager = LoginManager()
