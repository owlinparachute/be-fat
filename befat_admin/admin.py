# -*- coding: utf-8 -*-

from flask import Flask
from flask.ext.admin import Admin
from flask.ext.admin.contrib.mongoengine import ModelView
from models import User
from models import UserInfo
from models import Measurement
from extensions import db

def setup_admin(app):
    admin = Admin(app, u'Be Fat Admin')
    admin.add_view(ModelView(User))
    admin.add_view(ModelView(UserInfo))
    admin.add_view(ModelView(Measurement))
