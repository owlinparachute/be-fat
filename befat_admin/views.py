# -*- coding: utf-8 -*-

from flask import Blueprint

admin_handlers = Blueprint("admin_handlers", __name__)

@admin_handlers.errorhandler(403)
def forbidden_403(exception):
    return redirect(url_for('admin_handlers.login_view'))
