# -*- coding: utf-8 -*-

from extensions import db
from werkzeug.security import generate_password_hash, check_password_hash

__all__ = ['measurement']


class User(db.Document):
    username = db.StringField(unique=True, required=True)
    password = db.StringField(required=True)
    last_login = db.DateTimeField()

    def get_id(self):
        return self.username

    def set_password(self, password):
        self.password= generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.pw_hash, password)

class UserInfo(db.Document):
    user = db.IntField()
    email = db.EmailField(unique=True)
    active = db.BooleanField(default=True)
    created = db.DateTimeField()
    birthday = db.DateTimeField()
    gender = db.IntField()

class Measurement(db.Document):
    user = db.IntField()
    weight = db.FloatField()
    height = db.FloatField()
    waistline = db.FloatField()
    bfi = db.FloatField()
    bmi = db.FloatField()
